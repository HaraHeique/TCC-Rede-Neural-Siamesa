# Trabalho de Conclusão de Curso - Similaridades de Estilos Literários Utilizando Rede Neural Convolutiva Siamesa

Trabalho Conclusão referente ao curso de graduação de Bacharelado de Sistema de Informação do IFES - Serra.

### Informações gerais
- **Autor**: Harã Heique
- **Orientador**: Fidelis Castro
- **Linguagem de programação**: Python (versão 3.6.8+)
- **Ferramentas de suporte**: _Checar_
- **Ambiente de desenvolvimento**: Visual Studio Code (versão 1.35.1+)

### Informações adicionais
Todo o código fonte está hospedado no [GitHub](https://github.com/HaraHeique/TCC-Rede-Neural-Siamesa).
